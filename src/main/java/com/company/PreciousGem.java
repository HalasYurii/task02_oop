package com.company;

import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;

public class PreciousGem extends Gemstone {
    private static final String ABOUTSTONE = "\n"
            + "Gemstones are minerals, rocks or organic matter that have"
            + " been cut, polished and then fashioned into a piece of "
            + "jewelry. In the 1800s, these gemstones were divided into "
            + "two categories: precious and semi precious stones considering"
            + " their value differentiation at that time. ";

    public PreciousGem() {
        super();
        super.setUniqHistory(ABOUTSTONE);
    }

    public PreciousGem(final Set color,
                       final double carats,
                       final double priceInDollar,
                       final int transparencyInPerCent) {
        super(color, carats, priceInDollar, transparencyInPerCent);
        super.setUniqHistory(ABOUTSTONE);
    }

    @Override
    public final void handleTheStone() {
        System.out.println("Handle the stone to precious Gem");
    }

    @Override
    public final int hashCode() {
        return Objects.hash(
                super.getName(),
                super.getLooks(),
                super.getColor(),
                super.getCarats(),
                super.getPriceInDollar(),
                super.getTransparencyInPerCent(),
                super.getUniqHistory());
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PreciousGem other = (PreciousGem) obj;
        if (!super.getName().equals(other.getName())) {
            return false;
        }
        if (super.getColor() != other.getColor()) {
            return false;
        }
        if (super.getLooks().equals(other.getLooks())) {
            return false;
        }
        if (super.getCarats() != (other.getCarats())) {
            return false;
        }
        if (super.getPriceInDollar() != (other.getPriceInDollar())) {
            return false;
        }
        if (super.getTransparencyInPerCent()
                != (other.getTransparencyInPerCent())) {
            return false;
        }

        return true;
    }
}
