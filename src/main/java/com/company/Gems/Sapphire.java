package com.company.Gems;

import com.company.Enums.Color;
import com.company.PreciousGem;

import java.util.EnumSet;


/**
 * It's unique Gemstone with specific looks and name.
 */
public class Sapphire extends PreciousGem {
    /**
     * for field looks from Gemstone.
     */
    private static final String ABOUTLOOKS = "Like a piece of cosmos";
    /**
     * for name of the stone.
     */
    private static final String ABOUTNAME = "Sapphire";

    /**
     * default constructor with setting name and looks.
     */
    public Sapphire() {
        super();
    }
    /**
     * constructor that will set looks and name with parameters.
     *
     * @param color                 it's color of the gemstone
     * @param carats                1 carat = 200 milligrams
     * @param priceInDollar         price gemstone
     * @param transparencyInPerCent better this -> better stone
     */
    public Sapphire(
            final EnumSet<Color> color,
            final double carats,
            final double priceInDollar,
            final int transparencyInPerCent) {
        super(color, carats, priceInDollar, transparencyInPerCent);
        super.setName(ABOUTNAME);
        super.setLooks(ABOUTLOOKS);
    }
}
