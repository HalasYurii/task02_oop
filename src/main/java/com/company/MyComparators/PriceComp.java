package com.company.MyComparators;

import com.company.Gemstone;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Destination for this class is sorting by Price (double).
 */
public final class PriceComp implements Comparator<Gemstone>, Serializable {

    private static final long serialVersionUID = -5972458403679726498L;
    /**default constructor.*/
    public PriceComp(){

    }
    @Override
    public int compare(final Gemstone o1, final Gemstone o2) {
        return Double.compare(o1.getPriceInDollar(), o2.getPriceInDollar());
    }
}
