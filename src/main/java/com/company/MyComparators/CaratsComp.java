package com.company.MyComparators;

import com.company.Gemstone;

import java.io.Serializable;
import java.util.Comparator;


/**
 * Destination for this class is sorting by Name(String).
 */
public final class CaratsComp implements Comparator<Gemstone>, Serializable {
    private static final long serialVersionUID = -5972458403679726498L;

    /**
     * default constructor.
     */
    public CaratsComp() {
    }

    @Override
    public int compare(
            final Gemstone o1,
            final Gemstone o2) {
        return o1.getName().compareTo(o2.getName());
    }

}
