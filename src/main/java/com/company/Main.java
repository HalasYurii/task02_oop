package com.company;

import com.company.Enums.Color;
import com.company.Gems.Ametrine;
import com.company.Gems.Diamond;
import com.company.Gems.Sapphire;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.logging.Logger;

/**
 * main class from which start program.
 */
public final class Main {
    /**
     * default constructor.
     */
    private Main() {
    }
    /**
     * class from which you can controlling calls methods.
     *
     * @param args is first argument in cmd.
     */
    public static void main(final String[] args) {

        final ArrayList<Gemstone> gems = new ArrayList<>();

        Gemstone gem1 = new Ametrine(
                EnumSet.of(Color.ORANGE), 18.36, 81, 50);
        Gemstone gem2 = new Sapphire(
                EnumSet.of(Color.WHITE, Color.GREEN), 1.14, 275, 33);
        Gemstone gem3 = new Diamond(
                EnumSet.of(Color.RED, Color.YELLOW), 1.84, 100, 30);

        gem1.handleTheStone();
        gem2.handleTheStone();
        gem3.handleTheStone();

        gems.add(gem1);
        gems.add(gem2);
        gems.add(gem3);

        Necklace necklace = new Necklace(gems);
        System.out.println("Not sorted:\nName: "
                + necklace.getNameGems() + "\nPrice: "
                + necklace.getPriceGems() + "\n");

        necklace.sortByCarats();
        System.out.println("Sorted by carats:\nName: "
                + necklace.getNameGems() + "\nPrice: "
                + necklace.getPriceGems() + "\n"
                + "Max price gem: " + necklace.findMaxByPrice().getName());

        final int from = 30;
        final int to = 35;

        final Necklace found = new Necklace(
                necklace.findGemsByTransparency(from, to));
        System.out.println("Names gems that transparency start: "
                + from + ", end: " + to + "\nNames: " + found.getNameGems());

    }
}
