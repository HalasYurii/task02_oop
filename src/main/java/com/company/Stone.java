package com.company;

import com.company.Enums.Color;

import java.util.EnumSet;
import java.util.Set;

public abstract class Stone {
    private String name;
    private Set color;

    public Stone() {
        name = "";
        color = EnumSet.of(Color.NULL);
    }

    public Stone(final Set color) {
        this.color = color;
    }

    public abstract void handleTheStone();

    public final String getName() {
        return name;
    }

    protected final void setName(final String name) {
        this.name = name;
    }

    public final Set getColor() {
        return color;
    }

    public final void setColor(final Set color) {
        this.color = color;
    }


    public final String getInfo() {
        return "Stone{"
                + "name='" + name + '\''
                + ", color=" + color
                + "}\n";
    }

}
