package com.company;

import com.company.MyComparators.CaratsComp;
import com.company.MyComparators.PriceComp;

import java.util.ArrayList;
import java.util.Collections;

public class Necklace {
    private ArrayList<Gemstone> gems;

    public Necklace() {
    }

    public Necklace(final ArrayList<Gemstone> gems) {
        this.gems = gems;
    }

    public final String getNameGems() {
        StringBuilder allName = new StringBuilder();
        final int size = gems.size();
        for (int i = 0; i < size; i++) {
            allName.append(gems.get(i).getName());
            if (i != size - 1) {
                allName.append(", ");
            }
        }
        return allName.toString();
    }

    public final String getPriceGems() {
        StringBuilder allPrice = new StringBuilder();
        final int size = gems.size();
        for (int i = 0; i < size; i++) {
            allPrice.append(gems.get(i).getPriceInDollar());
            if (i != size - 1) {
                allPrice.append(", ");
            }
        }
        return allPrice.toString();
    }

    public final Gemstone findMaxByPrice() {
        return Collections.max(gems, new PriceComp());
    }

    public final ArrayList<Gemstone> findGemsByTransparency(
            final int from,
            final int to) {
        ArrayList<Gemstone> found = new ArrayList<>();
        for (Gemstone gem : gems) {
            if (gem.getTransparencyInPerCent()
                    <= to && gem.getTransparencyInPerCent() >= from) {
                found.add(gem);
            }
        }
        return found;
    }

    public final void sortByCarats() {
        Collections.sort(gems, new CaratsComp());
    }

}
