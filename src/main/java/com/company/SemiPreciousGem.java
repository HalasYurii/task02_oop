package com.company;

import com.company.Enums.Color;

import java.util.EnumSet;
import java.util.Objects;
import java.util.logging.Logger;

public class SemiPreciousGem extends Gemstone {
    private static final String ABOUTSTONE = "Today some semi precious "
            + "gemstones can be worth much more than a precious stone. ";

    public SemiPreciousGem() {
        super();
        super.setUniqHistory(ABOUTSTONE);
    }

    public SemiPreciousGem(final EnumSet<Color> color,
                           final double carats,
                           final double priceInDollar,
                           final int transparencyInPerCent) {
        super(color, carats, priceInDollar, transparencyInPerCent);
        super.setUniqHistory(ABOUTSTONE);
    }

    @Override
    public final void handleTheStone() {
        System.out.println("Handle the stone to semiprecious Gem");
    }

    @Override
    public final int hashCode() {
        return Objects.hash(
                super.getName(),
                super.getLooks(),
                super.getColor(),
                super.getCarats(),
                super.getPriceInDollar(),
                super.getTransparencyInPerCent(),
                super.getUniqHistory());
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SemiPreciousGem other = (SemiPreciousGem) obj;
        if (!super.getName().equals(other.getName())) {
            return false;
        }
        if (super.getColor() != other.getColor()) {
            return false;
        }
        if (super.getLooks().equals(other.getLooks())) {
            return false;
        }
        if (super.getCarats() != (other.getCarats())) {
            return false;
        }
        if (super.getPriceInDollar() != (other.getPriceInDollar())) {
            return false;
        }
        if (super.getTransparencyInPerCent()
                != (other.getTransparencyInPerCent())) {
            return false;
        }
        return true;
    }
}
