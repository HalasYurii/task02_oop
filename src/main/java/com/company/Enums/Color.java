package com.company.Enums;


/**
 * Colors that will use for class Gemstone.
 */
public enum Color {
    YELLOW, RED, GREEN, BLUE, GRAY, BLACK, WHITE, ORANGE, NULL;
}
