package com.company;

import java.util.Set;

/**
 * Abstract Class through it you can add some value.
 */
public abstract class Gemstone extends Stone{
    /**
     * 1 carat = 200 milligrams.
     */
    private double carats;
    /**
     * price for the gemstone.
     */
    private double priceInDollar;
    /**
     * transparency in percent.
     */
    private int transparencyInPerCent;
    /**
     * will fill up child class.
     */
    private String uniqHistory;
    /**
     * fill up child class.
     */
    private String looks;

    /**
     * default constructor with default values.
     */
    public Gemstone() {
        super();
        carats = 0;
        priceInDollar = 0;
        transparencyInPerCent = 0;
        looks = "";
    }

    /**
     * constructor with parameters.
     *
     * @param color                 color of the stone.
     * @param carats                1 carat = 200 milligrams.
     * @param priceInDollar         price for the stone.
     * @param transparencyInPerCent better this -> better price/looks/
     */
    public Gemstone(
            final Set color,
            final double carats,
            final double priceInDollar,
            final int transparencyInPerCent) {
        super(color);
        this.carats = carats;
        this.priceInDollar = priceInDollar;
        this.transparencyInPerCent = transparencyInPerCent;
    }

    /**
     * common function for get Carats, but not reinitialise.
     *
     * @return double value of carats.
     */
    public final double getCarats() {
        return carats;
    }

    /**
     * common function for only initialise field carats.
     * @param carats 1 carat = 200 milligrams.
     */
    public final void setCarats(final double carats) {
        this.carats = carats;
    }

    /**
     * common function for get price in dollar for the stone.
     *
     * @return double value about price.
     */
    public final double getPriceInDollar() {
        return priceInDollar;
    }

    /**
     * common function for set price for the stone.
     * @param priceInDollar price for the stone.
     */
    public final void setPriceInDollar(final double priceInDollar) {
        this.priceInDollar = priceInDollar;
    }

    /**
     * common function for get transparency in percent.
     *
     * @return short value in percent, transparency.
     */
    public final int getTransparencyInPerCent() {
        return transparencyInPerCent;
    }

    /**
     * common function for initialise transparency.
     * @param transparencyInPerCent short value of transparency.
     */
    public final void setTransparencyInPerCent(
            final int transparencyInPerCent) {
        this.transparencyInPerCent = transparencyInPerCent;
    }

    /**
     * function to set description of the stone,
     * initialise in child classes.
     *
     * @return history and some information of the stone.
     */
    public final String getUniqHistory() {
        return uniqHistory;
    }

    /**
     * function to set history and description of the stone.
     * @param uniqHistory initialise unique history.
     */
    public final void setUniqHistory(final String uniqHistory) {
        this.uniqHistory = uniqHistory;
    }

    /**
     * function to get looks of the stone.
     *
     * @return string, looks;
     */
    public final String getLooks() {
        return looks;
    }

    /**
     * function for set looks.
     * @param looks way initialise variable looks
     */
    protected final void setLooks(final String looks) {
        this.looks = looks;
    }

    /**
     * function that use in sout and also good for get information about object.
     * @return information in string value.
     */
    @Override
    public final String toString() {
        return super.getInfo() + "Gemstone{"
                + "carats=" + carats
                + ", price=" + priceInDollar
                + "$, transparencyInPerCent=" + transparencyInPerCent
                + "uniqHistory= \'" + uniqHistory
                + ", looks='" + looks + '\''
                + "}\n";
    }
}
